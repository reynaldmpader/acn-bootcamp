USE `acn_training`;

CREATE TABLE `user` (
  `id` varchar(60) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `message` (
  `id` varchar(60) NOT NULL,
  `message` varchar(255) DEFAULT NULL,
  `sender_id` varchar(60) DEFAULT NULL,
  `recipient_id` varchar(60) DEFAULT NULL,
  `date_sent` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_message_sender` (`sender_id`),
  KEY `fk_message_receiptient` (`recipient_id`),
  CONSTRAINT `fk_message_receiptient` FOREIGN KEY (`recipient_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_message_sender` FOREIGN KEY (`sender_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

INSERT INTO `user` (`id`, `username`, `password`) values ('83dce60c-5716-4eff-b687-272b3afabaf9','user','$2a$10$S3qTCkvoFdKn/VCKAi8hpOuO3hqFriRHxZXgJiwB/.Td1dUW5gpMK');
COMMIT;
