package com.accenture.training.data.repository;

import com.accenture.training.data.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rpader on 6/30/16. Edited by Jkida on 7/5/16
 */
public interface UserRepository extends CrudRepository<User, String> {

    User findByUsername(String username);

    void deleteByUsername(String username);

    @Query("SELECT COUNT(u) FROM User u WHERE u.username=?1")
    long countByName(String username);

}
