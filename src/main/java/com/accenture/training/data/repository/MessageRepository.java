package com.accenture.training.data.repository;

import com.accenture.training.data.Message;
import com.accenture.training.data.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by rpader on 6/30/16. Edited by JKida on 7/4/16.
 */
public interface MessageRepository extends CrudRepository<Message, String> {

    List<Message> findBySender(User user);

    List<Message> findByRecipient(User recipient);

    void deleteBySender(User sender);

    void deleteByRecipient(User recipient);
}
