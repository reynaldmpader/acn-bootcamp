package com.accenture.training.data;

import javax.persistence.*;

/**
 * Created by rpader on 6/30/16. Edited by JKida on 7/4/16.
 */
@Entity
@Table(name = "message")
public class Message {

    @Id
    private String id;

    @Column(name = "message")
    private String message;

    @Column(name = "date_sent")
    private String date;

    @ManyToOne
    @JoinColumn(name = "sender_id")
    private User sender;

    @ManyToOne
    @JoinColumn(name = "recipient_id")
    private User recipient;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public User getrecipient() {
        return recipient;
    }

    public void setrecipient(User recipient) {
        this.recipient = recipient;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }


}
