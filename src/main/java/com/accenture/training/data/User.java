package com.accenture.training.data;

import javax.persistence.*;
import java.util.List;

/**
 * Created by rpader on 7/4/16. * Edited by JKida on 7/4/16.
 */
@Entity
@Table(name = "user")
public class User {

    @Id
    private String id;

    @Column
    private String username;

    @Column
    private String password;

    @OneToMany(mappedBy = "sender",
               orphanRemoval = true)
    private List<Message> messageSender;

    @OneToMany(mappedBy = "recipient",
               orphanRemoval = true)
    private List<Message> messagerecipient;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
