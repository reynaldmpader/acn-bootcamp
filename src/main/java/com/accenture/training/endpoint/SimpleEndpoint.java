package com.accenture.training.endpoint;

import com.accenture.training.MessageRequest;
import com.accenture.training.data.Message;
import com.accenture.training.data.User;
import com.accenture.training.data.repository.MessageRepository;
import com.accenture.training.data.repository.UserRepository;
import com.accenture.training.service.SimpleService;
import com.accenture.training.service.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by rpader on 7/7/16.
 */
@RestController
public class SimpleEndpoint {

    @RequestMapping("/hello")
    public User hello() {
        return getCurrentUser();
    }

    @RequestMapping("/anonymous-hello")
    public String anon() {
        return "Hello unknown";
    }

    public User getCurrentUser() {
        Object principal = SecurityContextHolder.getContext()
                                                .getAuthentication()
                                                .getPrincipal();
        if (principal instanceof UserDetails) {
            return ((UserDetailsImpl) principal).getUser();
        } else {
            return null;
        }
    }

///--------------------------------------------


    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SimpleService simpleService;

    @RequestMapping(value = "/send",
                    method = RequestMethod.POST)
    public void save(@RequestParam String userSender, @RequestParam String userrecipient,
                     @RequestBody MessageRequest sample) {
        simpleService.sendMessage(userSender, userrecipient, sample);
    }

    @RequestMapping(value = "/{username}/messages/sent",
                    method = RequestMethod.GET)
    public List<Message> sentMessages(@PathVariable("username") String username) {
        return simpleService.getSentMessages(username);
    }

    @RequestMapping(value = "/{username}/messages/inbox",
                    method = RequestMethod.GET)
    public List<Message> recievedMessages(@PathVariable("username") String username) {
        return simpleService.getRecievedMessages(username);
    }
}
