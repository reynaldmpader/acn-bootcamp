package com.accenture.training.service;

import com.accenture.training.MessageRequest;
import com.accenture.training.data.Message;
import com.accenture.training.data.User;
import com.accenture.training.data.repository.MessageRepository;
import com.accenture.training.data.repository.UserRepository;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * Created by rpader on 7/5/16.
 */
@Service
public class SimpleService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageRepository messageRepository;

    public void registerUser(User username) {
        Long exist = userRepository.countByName(username.getUsername());

        if (exist < 1) {
            User user = new User();
            user.setId(UUID.randomUUID().toString());
            user.setUsername(username.getUsername());
            userRepository.save(user);
        } else {
            throw new RuntimeException("User already exists");
        }
    }

    public void sendMessage(String userSender, String userrecipient, MessageRequest sample) {
        User sender = userRepository.findByUsername(userSender);
        User recipient = userRepository.findByUsername(userrecipient);
        Message msg = new Message();
        msg.setId(UUID.randomUUID().toString());
        msg.setMessage(sample.getMessage());
        msg.setSender(sender);
        msg.setrecipient(recipient);
        msg.setDate(DateTime.now().toString("MM/dd/yyyy"));
        messageRepository.save(msg);
    }

    public List<Message> getSentMessages(String username) {
        User sender = userRepository.findByUsername(username);
        return messageRepository.findBySender(sender);
    }

    public List<Message> getRecievedMessages(String username) {
        User recipient = userRepository.findByUsername(username);
        return messageRepository.findByRecipient(recipient);
    }
}
