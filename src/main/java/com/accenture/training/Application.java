package com.accenture.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCrypt;

/**
 * Created by rpader on 6/28/16. Edited by JKida on 7/4/16.
 */
@SpringBootApplication
@EntityScan
@EnableJpaRepositories
@EnableWebSecurity
public class Application {

    public static void main(String[] args) {
        System.out.println("BCrypt of 'password'=================================="+ BCrypt.hashpw("password",BCrypt.gensalt()));
        SpringApplication.run(Application.class, args);
    }



}

