package com.accenture.training;

/**
 * Created by rpader on 6/30/16. Edited by JKida on 7/4/16.
 */
public class MessageRequest {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
